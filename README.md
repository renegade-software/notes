**Project Name**: Notes

**Project Description**: An open-source note-taking app that is secure, intuitive, and cross-platform.

**Current Progress**:

*  Note-taking with basic text
*  Create checklists

**Key Development Milestones**:

* Note Taking capability with rich text, images, audio, and video.
* Customizable checklists.
* Stylus drawing support, with canvas texture option.
* Password encryption option.
* Notes search option.
* Scan images option.
* OCR option for scanned images.
* Cloud syncing.
* List sharing option.
* Multi-theme support.
* Linux, Android, FreeBSD, iOS, MacOS and Windows clients.
* Support for file attachments.
* Minimalist interface.
* Version History.
* Date and time reminder options.
* Tags option for classifying notes.
* Clip webpages option.
* Universal drag 'n' drop functionality.
* Note card pinning option.
* Unique card flip notebook interface.

**Pull Requests**:

You're welcome to fix any issues or help enhance the functionality of the Notes app. You may contact https://gitlab.com/prajwal_dsouza for any info on the development of this project.